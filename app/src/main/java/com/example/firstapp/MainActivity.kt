package com.example.firstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.firstapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.button.setOnClickListener(this)

        /*
        binding.button.setOnClickListener {
            var message : String
            var n = binding.editText.text.toString().toInt()
            if (n % 2 == 0) {
                message = "par"
            } else {
                message = "impar"
            }

            binding.textView.text = "El número $n es $message"
        }
        */
    }

    override fun onClick(p0: View?) {
        if (p0 == binding.button) {
            var message : String = ""
            var n = 1

            if (binding.editText.text.isEmpty()) {
                message =" Debe introducir un número"
            } else {
                n = binding.editText.text.toString().toInt()
                if (n % 2 == 0) {
                    message = "El número $n es par"
                } else {
                    message = "El número $n es impar"
                }
            }
            binding.textView.text = message
            /*
            var error : Boolean = false
            try {
                n = binding.editText.text.toString().toInt()
            } catch (e : NumberFormatException) {
                message = "Error: $e.message.toString()"
                Log.e("Error", message)
                error = true
            }

            if (!error) {
                if (n % 2 == 0) {
                    message = "El número $n es par"
                } else {
                    message = "El número $n es impar"
                }
            }

            binding.textView.text = message
             */
        }
    }
}
